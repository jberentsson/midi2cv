#include <xc.h>
#include "midi2cv.h"
#include "seq.h"
#include "led.h"
#include "eeprom.h"

#define PULSE_DURATION  2 // Milliseconds, approximately

#define PIN_SYNC        PIN_A0
#define PIN_RUN_STOP    PIN_B4
#define PIN_RESET       PIN_B3

//  Div2    Div3    Index   Divider
//  0 (1)   0 (1)   0       1
//  0 (1)   1 (3)   1       3
//  1 (2)   0 (1)   2       2
//  1 (2)   1 (3)   3       6
//  2 (4)   0 (1)   4       4
//  2 (4)   1 (3)   5       12
//  3 (8)   0 (1)   6       8
//  3 (8)   1 (3)   7       24
static const uint8_t Dividers[] = {1, 3, 2, 6, 4, 12, 8, 24};

static uint8_t g_SyncCounter;
static uint8_t g_LedCounter;
static uint8_t g_PulseLengthCounter;

void Seq_Init(void)
{
    g_SyncCounter = 1;
    g_LedCounter = 1;
    g_PulseLengthCounter = 0;
    
    PIN_SYNC = 0;
    PIN_RUN_STOP = 0;
    PIN_RESET = 0;
}

void Seq_IncrementDiv2(void)
{
    SETTINGS(divider) += 2;
    SETTINGS(divider) &= 0x7;
}

void Seq_ToggleDiv3(void)
{
    SETTINGS(divider) ^= 0x01;
}

void Seq_ClockTick(void)
{
    g_SyncCounter--;
    g_LedCounter--;
    
    if (g_LedCounter == 0)
    {
        LED_Set(LED_ON);
        g_LedCounter = 24;
    }
    else if (g_LedCounter == 12)
    {
        LED_Set(LED_OFF);
    }
    
    if (g_SyncCounter == 0)
    {
        di();
        g_PulseLengthCounter = PULSE_DURATION;
        PIN_SYNC = 1;
        ei();
        
        g_SyncCounter = Dividers[SETTINGS(divider)];
    }
}

void Seq_ClockReset(void)
{
    g_SyncCounter = 1;
    g_LedCounter = 1;
}

void Seq_Start(void)
{
    di();
    PIN_RUN_STOP = 1;
    PIN_RESET = 1;
    ei();
}

void Seq_Continue(void)
{
    di();
    PIN_RUN_STOP = 1;
    PIN_RESET = 0;
    ei();
}

void Seq_Stop(void)
{
    di();
    PIN_RUN_STOP = 0;
    LED_Set(LED_OFF);
    ei();
}

inline void Seq_Tick()
{
    // We don't need to disable interrupts for RMWs because
    // this function is only called from the interrupt.
    
    if (g_PulseLengthCounter > 0)
    {
        if (--g_PulseLengthCounter == 0)
        {
            PIN_SYNC = 0;
            PIN_RESET = 0;
        }
    }
}

