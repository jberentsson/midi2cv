#ifndef SEQ_H_
#define SEQ_H_

void Seq_Init(void);

void Seq_IncrementDiv2(void);
void Seq_ToggleDiv3(void);

void Seq_ClockTick(void);
void Seq_ClockReset(void);

void Seq_Start(void);
void Seq_Continue(void);
void Seq_Stop(void);

inline void Seq_Tick(void);


#endif

