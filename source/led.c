#include <xc.h>
#include "led.h"
#include "midi2cv.h"

#define LED_PIN     PIN_A7

static uint8_t g_UnforcedState;
bool_t g_LedIsForced;

void LED_Init(void)
{
    LED_Set(LED_OFF);
    LED_Unforce();
}

void LED_Set(uint8_t state)
{
    g_UnforcedState = state;
    
    di();
    if (g_LedIsForced == false)
        LED_PIN = state;
    ei();
}

void LED_Force(uint8_t state)
{
    g_LedIsForced = true;
    
    di();
    LED_PIN = state;
    ei();
}

void LED_Unforce(void)
{
    g_LedIsForced = false;
    
    di();
    LED_PIN = g_UnforcedState;
    ei();
}

void LED_Blink(uint8_t t)
{
    uint8_t i;
    
    LED_Force(LED_OFF);
        
    for (i = 0; i < t; i++)
    {
        LED_Force(LED_ON);
        __delay_ms(125);
        LED_Force(LED_OFF);
        __delay_ms(125);
    }
    
    LED_Unforce();
}

