#include <xc.h>
#include "midi2cv.h"
#include "dac.h"

#define DAC_SCK     PIN_A4
#define DAC_SDI     PIN_A2
#define DAC_CS0     PIN_A3
#define DAC_CS1     PIN_A1

void DAC_Init(void)
{
    DAC_SCK = 0;
    DAC_CS0 = 1;
    DAC_CS1 = 1;
}

void DAC_Set(uint8_t dac, uint16_t code)
{
    uint8_t cs0 = (dac & 0x2) >> 1;
    
    // In case this is unit 2
    dac &= 0x3;
    
    // Select which DAC on the chip
    code |= dac & 0x1 ? 0x9000 : 0x1000;
    
    // Disable interrupts
    di();
    
    // Select which chip
    DAC_CS0 = cs0;
    DAC_CS1 = !cs0;
    
    // Bitbanged SPI
    // A loop is prettier but it's way faster unrolled
#define BITBANG code <<= 1; DAC_SCK = 0; DAC_SDI = STATUSbits.CARRY; DAC_SCK = 1;
    BITBANG
    BITBANG
    BITBANG
    BITBANG
    BITBANG
    BITBANG
    BITBANG
    BITBANG
    BITBANG
    BITBANG
    BITBANG
    BITBANG
    BITBANG
    BITBANG
    BITBANG
    BITBANG
    
    asm("nop"); // stretch the last clock pulse a little bit
    DAC_SCK = 0;
    
    // Deselect both chips
    DAC_CS0 = 1;
    DAC_CS1 = 1;
    
    // Enable interrupts
    ei();
}

