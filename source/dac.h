#ifndef DAC_H_
#define DAC_H_

#include "midi2cv.h"

#define DAC_A   0
#define DAC_B   1
#define DAC_C   2
#define DAC_D   3

#define DAC_MONO_NOTE       DAC_A
#define DAC_MONO_CV         DAC_B
#define DAC_MONO_VELOCITY   DAC_C
#define DAC_MONO_MOD        DAC_D

#define DAC_ZERO_SCALE      0x000
#define DAC_FULL_SCALE      0xFFF

void DAC_Init(void);
void DAC_Set(uint8_t dac, uint16_t code);

#endif

