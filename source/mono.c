// The monophonic voice allocation logic in this file is based on note_stack.h 
// from the Mutable Instruments CVpal firmware, designed by Olivier Gillet.
// http://mutable-instruments.net/modules/cvpal
// https://github.com/pichenettes/cvpal/tree/master/cvpal
//
// -----------------------------------------------------------------------------
//
// Copyright 2011 Olivier Gillet.
//
// Author: Olivier Gillet (ol.gillet@gmail.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// -----------------------------------------------------------------------------
//
// Stack of currently pressed keys.
//
// Currently pressed keys are stored as a linked list. The linked list is used
// as a LIFO stack to allow monosynth-like behaviour. An example of such
// behaviour is:
// player presses and holds C4-> C4 is played.
// player presses and holds C5 (while holding C4) -> C5 is played.
// player presses and holds G4 (while holding C4&C5)-> G4 is played.
// player releases C5 -> G4 is played.
// player releases G4 -> C4 is played.
//
// The nodes used in the linked list are pre-allocated from a pool of 16
// nodes, so the "pointers" (to the root element for example) are not actual
// pointers, but indices of an element in the pool.
//
// Additionally, an array of pointers is stored to allow random access to the
// n-th note, sorted by ascending order of pitch (for arpeggiation).

#include <xc.h>
#include "mono.h"
#include "dac.h"
#include "eeprom.h"
#include "led.h"
#include "midi2cv.h"
#include "seq.h"
#include "calibrate.h"


#define PIN_GATE            PIN_B7
#define PIN_TRIG            PIN_B6

#define FREE_SLOT           0xFF
#define CAPACITY            8

#define PITCHBEND_MIDPOINT  64
#define RETRIGGER_DURATION  2 // Milliseconds, approximately

#define MIDI_CC_MOD                     1
#define MIDI_CC_SUSTAIN                 64
#define MIDI_CC_ALL_SOUND_OFF           120
#define MIDI_CC_RESET_ALL_CONTROLLERS   121
#define MIDI_CC_ALL_NOTES_OFF           123

#define NOTE_CV2_LEARN  12 // C1.  Enters CV2 learn mode
#define NOTE_DIV3       13 // C#1. Toggles software divider between /1 and /3
#define NOTE_RETRIGGER  14 // D1.  Toggles retrigger
#define NOTE_DIV2       15 // D#1. Selects software /2 divider state thru /1, /2, /4 and /8
#define NOTE_EXP_VEL    16 // E1.  Toggles exponential velocity
#define NOTE_REGATE     18 // F#1. Toggles regate
#define NOTE_CHPRESSURE 19 // G1.  Assigns Aftertouch to CV2
#define NOTE_CALIBRATE  20 // G#1. Starts software calibration procedure
#define NOTE_LOCK       22 // A#1. Enables lock of the function keys till next restart
#define NOTE_SAVE       23 // B1.  Saves parameters.


typedef struct NoteEntry
{
    uint8_t note;
    uint8_t velocity;
    uint8_t next_index;  // 1-based
} NoteEntry_t;

static NoteEntry_t g_NotePool[CAPACITY + 1];  // First element is a dummy node!
static uint8_t g_PoolSize;
static uint8_t g_TopIndex;  // Base 1.

static uint8_t g_Pitchbend; ///< Upper 7 bits of 14-bit pitchbend data
static bool_t g_CV2Learn;
static uint8_t g_RetrigCounter;
static uint8_t g_RegateCounter;
static bool_t g_FunctionKeysAreLocked;
static bool_t g_PedalIsHeld;

static const uint8_t g_ExpTable[128] =
{
   0,   1,   1,   1,   1,   1,   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,
   3,   3,   3,   3,   3,   3,   3,   4,   4,   4,   4,   4,   4,   5,   5,   5,
   5,   5,   6,   6,   6,   6,   7,   7,   7,   7,   8,   8,   8,   8,   9,   9,
   9,  10,  10,  11,  11,  11,  12,  12,  13,  13,  13,  14,  14,  15,  15,  16,
  17,  17,  18,  18,  19,  20,  20,  21,  22,  22,  23,  24,  25,  25,  26,  27,
  28,  29,  30,  31,  32,  33,  34,  35,  37,  38,  39,  40,  42,  43,  44,  46,
  47,  49,  50,  52,  54,  56,  57,  59,  61,  63,  65,  67,  69,  72,  74,  76,
  79,  81,  84,  87,  90,  93,  96,  99, 102, 105, 108, 112, 116, 119, 123, 127
};

//==============================================================================

static void NoteStack_NoteOn(uint8_t note, uint8_t velocity);
static void NoteStack_NoteOff(uint8_t note);
static void NoteStack_Clear(void);

static void UpdateNoteCV(void)
{
    int16_t code;
    
    code = Calibrate_GetCalibratedPitch(DAC_MONO_NOTE, g_NotePool[g_TopIndex].note);
    code = code + g_Pitchbend - 64;
    
    if (code < DAC_ZERO_SCALE)
    {
        code = DAC_ZERO_SCALE;
    }
    else if (code > DAC_FULL_SCALE)
    {
        code = DAC_FULL_SCALE;
    }
    
    DAC_Set(DAC_MONO_NOTE, code);
    DAC_Set(DAC_MONO_VELOCITY, g_NotePool[g_TopIndex].velocity << 5);
    
    if (SETTINGS(cv2_mode) == SETTINGS_CV2_MODE_NOTE)
    {
        code = Calibrate_GetCalibratedPitch(DAC_MONO_CV, g_NotePool[g_TopIndex].note);
        code = code + g_Pitchbend - 64;
    
        if (code < DAC_ZERO_SCALE)
        {
            code = DAC_ZERO_SCALE;
        }
        else if (code > DAC_FULL_SCALE)
        {
            code = DAC_FULL_SCALE;
        }
        
        DAC_Set(DAC_MONO_CV, code);
    }
}

//==============================================================================

void Mono_Init(void)
{
    NoteStack_Clear();
    
    PIN_GATE = 0;
    PIN_TRIG = 0;
    
    g_Pitchbend = PITCHBEND_MIDPOINT;
    g_CV2Learn = false;
    g_RetrigCounter = 0;
    g_RegateCounter = 0;
    g_FunctionKeysAreLocked = false;
    g_PedalIsHeld = false;
}

void Mono_NoteOn(uint8_t note, uint8_t velocity)
{
    if (note >= 24)
    {
        if (g_CV2Learn == true)
        {
            SETTINGS(cv2_mode) = SETTINGS_CV2_MODE_NOTE;
            g_CV2Learn = false;
            LED_Unforce();
        }
        
        if (velocity == 0)
        {
            Mono_NoteOff(note, velocity);
        }
        else
        {
            if (SETTINGS(exp_velocity) == true)
                velocity = g_ExpTable[velocity];
            
            // Check this here so that a note on for a note that
            // is already on will cause a regate
            bool_t legato = (g_PoolSize > 0);
            
            NoteStack_NoteOn(note, velocity);
            UpdateNoteCV();
            
            di();
            
            // Always trigger at first note on
            // Trigger at all note ons if retrigger mode is enabled
            if (legato == false || SETTINGS(retrigger) == true)
            {
                g_RetrigCounter = RETRIGGER_DURATION;
                PIN_TRIG = 1;
            }
            
            // Regate only at stacked note ons
            if (legato == true && SETTINGS(regate) == true)
            {
                g_RegateCounter = RETRIGGER_DURATION;
                PIN_GATE = 0;
            }
            else
            {
                PIN_GATE = 1;
            }
            
            ei();
        }
    }
    else if (SETTINGS(persistent_lock) == false && g_FunctionKeysAreLocked == false && velocity > 0)
    {
        // Cancel CV2 learn mode if another function key is pressed
        g_CV2Learn = false;
        LED_Unforce();
        
        switch (note)
        {
        case NOTE_CV2_LEARN:
            g_CV2Learn = true;
            LED_Force(LED_ON);
            break;
        case NOTE_DIV3:
            Seq_ToggleDiv3();
            break;
        case NOTE_RETRIGGER:
            SETTINGS(retrigger) = !SETTINGS(retrigger);
            break;
        case NOTE_DIV2:
            Seq_IncrementDiv2();
            break;
        case NOTE_EXP_VEL:
            SETTINGS(exp_velocity) = !SETTINGS(exp_velocity);
            break;
        case NOTE_REGATE:
            SETTINGS(regate) = !SETTINGS(regate);
            break;
        case NOTE_CHPRESSURE:
            SETTINGS(cv2_mode) = SETTINGS_CV2_MODE_CHPRESSURE;
            break;
        case NOTE_LOCK:
            g_FunctionKeysAreLocked = true;
            break;
        case NOTE_SAVE:
            if (EEPROM_SaveSettings())
                LED_Blink(2);
            break;
        }
    }
}

void Mono_NoteOff(uint8_t note, uint8_t velocity)
{
    if (note < 24)
        return;
    
    // Write to the dummy node so that when all keys are released, the
    // CVs will keep their previous values
    g_NotePool[0].note = g_NotePool[g_TopIndex].note;
    g_NotePool[0].velocity = g_NotePool[g_TopIndex].velocity;
    
    uint8_t top_note = g_NotePool[g_TopIndex].note;
    NoteStack_NoteOff(note);
    
    // Retrigger logic lives here
    di();
    if (g_PoolSize > 0)
    {
        UpdateNoteCV();
        
        if (g_NotePool[g_TopIndex].note != top_note)
        {
            if (SETTINGS(retrigger) == true)
            {
                g_RetrigCounter = RETRIGGER_DURATION;
                PIN_TRIG = 1;
            }
            
            if (SETTINGS(regate) == true)
            {
                g_RegateCounter = RETRIGGER_DURATION;
                PIN_GATE = 0;
            }
        }
    }
    else
    {
        if (g_PedalIsHeld == false)
        {
            PIN_GATE = 0;
            
            // Clear the regate counter so that the timer callback
            // doesn't reopen the gate.
            g_RegateCounter = 0;
        }
    }
    ei();
}

void Mono_Pitchbend(uint8_t lsbits, uint8_t msbits)
{
    g_Pitchbend = msbits;
    UpdateNoteCV();
}

void Mono_ControlChange(uint8_t control, uint8_t value)
{
    uint16_t code;
    
    // First check if this is a channel mode message
    if (control >= 120)
    {
        if (control == MIDI_CC_ALL_SOUND_OFF && value == 0)
        {
            g_PedalIsHeld = false;
            NoteStack_Clear();
            Mono_NoteOff(0, 0); // This is kinda hacky but it will update the gates
        }
        else if (control == MIDI_CC_RESET_ALL_CONTROLLERS && value == 0)
        {
            g_CV2Learn = false;
            g_Pitchbend = PITCHBEND_MIDPOINT;
            
            // TODO: this code is duplicated. Maybe move it to a pedal function.
            g_PedalIsHeld = false;
            if (g_PoolSize == 0)
            {
                di();
                
                PIN_GATE = 0;
                
                // Clear the regate counter so that the timer callback
                // doesn't reopen the gate.
                g_RegateCounter = 0;
                
                ei();
            }
        }
        else if (control == MIDI_CC_ALL_NOTES_OFF && value == 0)
        {
            NoteStack_Clear();
            Mono_NoteOff(0, 0); // This is kinda hacky but it will update the gates
        }
        
        return;
    }
    
    if (g_CV2Learn == true)
    {
        SETTINGS(cv2_mode) = SETTINGS_CV2_MODE_CC;
        SETTINGS(cv2_cc) = control;
        g_CV2Learn = false;
        LED_Unforce();
    }
    
    if (control == MIDI_CC_MOD)
    {
        code = value << 5;
        DAC_Set(DAC_MONO_MOD, code);
    }
    else if (control == MIDI_CC_SUSTAIN)
    {
        // On if value > 63
        if (value & 0x40)
        {
            g_PedalIsHeld = true;
        }
        else
        {
            g_PedalIsHeld = false;
            if (g_PoolSize == 0)
            {
                di();
                
                PIN_GATE = 0;
                
                // Clear the regate counter so that the timer callback
                // doesn't reopen the gate.
                g_RegateCounter = 0;
                
                ei();
            }
        }
    }
    
    if (SETTINGS(cv2_mode) == SETTINGS_CV2_MODE_CC && control == SETTINGS(cv2_cc))
    {
        code = value << 5;
        DAC_Set(DAC_MONO_CV, code);
    }
}

void Mono_ChannelPressure(uint8_t pressure)
{
    uint16_t code;
    
    if (SETTINGS(cv2_mode) == SETTINGS_CV2_MODE_CHPRESSURE)
    {
        code = pressure << 5;
        DAC_Set(DAC_MONO_CV, code);
    }
}

inline void Mono_Tick(void)
{
    // We don't need to disable interrupts for RMWs because
    // this function is only called from the interrupt.
    
    if (g_RetrigCounter > 0)
    {
        if (--g_RetrigCounter == 0)
        {
            PIN_TRIG = 0;
        }
    }
    
    if (g_RegateCounter > 0)
    {
        // Make sure at least one note is still held
        if (--g_RegateCounter == 0 && g_PoolSize > 0)
        {
            PIN_GATE = 1;
        }
    }
}

//==============================================================================

static void NoteStack_NoteOn(uint8_t note, uint8_t velocity)
{
    // Remove the note from the list first (in case it is already here).
    NoteStack_NoteOff(note);
    
    // In case of saturation, remove the least recently played note from the stack.
    if (g_PoolSize == CAPACITY)
    {
        uint8_t least_recent_note;
        for (uint8_t i = 1; i <= CAPACITY; ++i)
        {
            if (g_NotePool[i].next_index == NULL)
            {
                least_recent_note = g_NotePool[i].note;
            }
        }
        NoteStack_NoteOff(least_recent_note);
    }
    
    // Now we are ready to insert the new note. Find a free slot to insert it.
    uint8_t free_slot;
    for (uint8_t i = 1; i <= CAPACITY; ++i)
    {
        if (g_NotePool[i].note == FREE_SLOT)
        {
            free_slot = i;
            break;
        }
    }
    g_NotePool[free_slot].next_index = g_TopIndex;
    g_NotePool[free_slot].note = note;
    g_NotePool[free_slot].velocity = velocity;
    g_TopIndex = free_slot;
    g_PoolSize++;
}

static void NoteStack_NoteOff(uint8_t note)
{
    uint8_t current = g_TopIndex;
    uint8_t previous = NULL;
    
    while (current)
    {
        if (g_NotePool[current].note == note)
        {
            break;
        }
        previous = current;
        current = g_NotePool[current].next_index;
    }
    
    if (current)
    {
        if (previous)
            g_NotePool[previous].next_index = g_NotePool[current].next_index;
        else
            g_TopIndex = g_NotePool[current].next_index;
        
        g_NotePool[current].note = FREE_SLOT;
        g_PoolSize--;
    }
}

static void NoteStack_Clear(void)
{
    g_PoolSize = 0;
    g_TopIndex = NULL;
    
    // Initialize dummy node to known values
    // The dummy node is used to hold CVs after all keys are released
    g_NotePool[0].note = 24;
    g_NotePool[0].velocity = 64;
    
    for (uint8_t i = 1; i <= CAPACITY; ++i)
    {
        g_NotePool[i].note = FREE_SLOT;
    }
}

