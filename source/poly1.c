// Copyright 2012 Olivier Gillet.
//
// Author: Olivier Gillet (ol.gillet@gmail.com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// See http://creativecommons.org/licenses/MIT/ for more information.
//
// -----------------------------------------------------------------------------
//
// Polyphonic voice allocator.

#include <xc.h>
#include "poly1.h"
#include "dac.h"
#include "eeprom.h"
#include "led.h"
#include "midi2cv.h"
#include "seq.h"
#include "calibrate.h"


#define PIN_GATE1           PIN_B7
#define PIN_GATE2           PIN_B6
#define PIN_GATE3           PIN_B4
#define PIN_GATE4           PIN_B3

#define CAPACITY            8

#define PITCHBEND_MIDPOINT  64
#define RETRIGGER_DURATION  2 // Milliseconds, approximately

#define MIDI_CC_MOD                     1
#define MIDI_CC_SUSTAIN                 64
#define MIDI_CC_ALL_SOUND_OFF           120
#define MIDI_CC_RESET_ALL_CONTROLLERS   121
#define MIDI_CC_ALL_NOTES_OFF           123

#define NOTE_POLY_LIMIT 17 // F1.  Changes polyphony limit
#define NOTE_CALIBRATE  20 // G#1. Starts software calibration procedure
#define NOTE_LOCK       22 // A#1. Enables lock of the function keys till next restart
#define NOTE_SAVE       23 // B1.  Saves parameters.

static uint8_t g_Pitchbend; ///< Upper 7 bits of 14-bit pitchbend data
static uint8_t g_RegateCounter;
static bool_t g_FunctionKeysAreLocked;
static bool_t g_PedalIsHeld;
static uint8_t g_GatePinMask[4] = {0x80, 0x40, 0x10, 0x08};
static uint8_t g_NumVoices;

#define NOT_ALLOCATED   0xFF
#define ACTIVE_NOTE     0x80
#define NOTE_MASK       0x7F

static uint8_t g_Pool[CAPACITY];
// Holds the indices of the voices sorted by most recent usage.
static uint8_t g_LRU[CAPACITY];

//==============================================================================

static void RefreshOutput(uint8_t voice)
{
    if (g_Pool[voice] & ACTIVE_NOTE)
    {
        int16_t code;
        
        code = Calibrate_GetCalibratedPitch(voice, g_Pool[voice] & NOTE_MASK);
        code = code + g_Pitchbend - 64;
        
        if (code < DAC_ZERO_SCALE)
        {
            code = DAC_ZERO_SCALE;
        }
        else if (code > DAC_FULL_SCALE)
        {
            code = DAC_FULL_SCALE;
        }
        
        DAC_Set(voice, code);
        di();
#ifdef __16F88
        PORTB |= g_GatePinMask[voice];
#endif
#ifdef __16F1847
        LATB |= g_GatePinMask[voice];
#endif
        ei();
    }
    else if (g_PedalIsHeld == false)
    {
        di();
#ifdef __16F88
        PORTB &= ~g_GatePinMask[voice];
#endif
#ifdef __16F1847
        LATB &= ~g_GatePinMask[voice];
#endif
        ei();
    }
}

static void Touch(uint8_t voice)
{
    int8_t source = CAPACITY - 1;
    int8_t destination = CAPACITY - 1;

    while (source >= 0)
    {
        if (g_LRU[source] != voice)
        {
            g_LRU[destination--] = g_LRU[source];
        }

        --source;
    }

    g_LRU[0] = voice;
}

static uint8_t Find(uint8_t note)
{
    uint8_t i;
    
    for (i = 0; i < g_NumVoices; ++i)
    {
        if ((g_Pool[i] & NOTE_MASK) == note)
        {
            return i;
        }
    }

    return NOT_ALLOCATED;
}

static void Clear()
{
    uint8_t i;
    
    for (i = 0; i < CAPACITY; ++i)
    {
        g_Pool[i] = 0;
        g_LRU[i] = CAPACITY - i - 1;
    }
    
    PIN_GATE1 = 0;
    PIN_GATE2 = 0;
    PIN_GATE3 = 0;
    PIN_GATE4 = 0;
}

//==============================================================================

void Poly1_Init(void)
{
    g_Pitchbend = PITCHBEND_MIDPOINT;
    g_RegateCounter = 0;
    g_FunctionKeysAreLocked = false;
    g_PedalIsHeld = false;
    
    g_NumVoices = SETTINGS(num_voices);
    Clear();
}

void Poly1_NoteOn(uint8_t note, uint8_t velocity)
{
    if (note >= 24)
    {
        if (velocity == 0)
        {
            Poly1_NoteOff(note, velocity);
        }
        else
        {
            if (g_NumVoices == 0)
            {
                return;
            }

            // First, check if there is a voice currently playing this note. In this
            // case, this voice will be responsible for retriggering this note.
            // Hint: if you're more into string instruments than keyboard instruments,
            // you can safely comment those lines.
            uint8_t voice = Find(note);
            
            uint8_t i;

            // Then, try to find the least recently touched, currently inactive voice.
            if (voice == NOT_ALLOCATED)
            {
                for (i = 0; i < CAPACITY; ++i)
                {
                    if (g_LRU[i] < g_NumVoices && !(g_Pool[g_LRU[i]] & ACTIVE_NOTE))
                    {
                        voice = g_LRU[i];
                    }
                }
            }

            // If all voices are active, use the least recently played note
            // (voice-stealing).
            if (voice == NOT_ALLOCATED)
            {
                for (i = 0; i < CAPACITY; ++i)
                {
                    if (g_LRU[i] < g_NumVoices)
                    {
                        voice = g_LRU[i];
                    }
                }
            }

            g_Pool[voice] = note | ACTIVE_NOTE;
            Touch(voice);
            RefreshOutput(voice);
        }
    }
    else if (SETTINGS(persistent_lock) == false && g_FunctionKeysAreLocked == false && velocity > 0)
    {
        switch (note)
        {
        case NOTE_POLY_LIMIT:
            if (SETTINGS(stack_mode) == SETTINGS_STACK_MODE_NONE)
            {
                switch (SETTINGS(num_voices))
                {
                    case 2:  g_NumVoices = 3; break;
                    case 3:  g_NumVoices = 4; break;
                    case 4:  g_NumVoices = 2; break;
                    default: g_NumVoices = 4;
                }
                
                SETTINGS(num_voices) = g_NumVoices;
                Poly1_Init();
                
            }
            break;
        case NOTE_LOCK:
            g_FunctionKeysAreLocked = true;
            break;
        case NOTE_SAVE:
            if (EEPROM_SaveSettings())
                LED_Blink(2);
            break;
        }
    }
}

void Poly1_NoteOff(uint8_t note, uint8_t velocity)
{
    uint8_t voice = Find(note);

    if (voice != NOT_ALLOCATED)
    {
        g_Pool[voice] &= NOTE_MASK;
        Touch(voice);
        RefreshOutput(voice);
    }
}

void Poly1_Pitchbend(uint8_t lsbits, uint8_t msbits)
{
    g_Pitchbend = msbits;
    
    uint8_t i;
    
    for (i = 0; i < g_NumVoices; i++)
    {
        RefreshOutput(i);
    }
}

void Poly1_ChannelPressure(uint8_t pressure)
{
    
}

void Poly1_ControlChange(uint8_t control, uint8_t value)
{
    uint16_t code;
    
    // First check if this is a channel mode message
    if (control >= 120)
    {
        if (control == MIDI_CC_ALL_SOUND_OFF && value == 0)
        {
            g_PedalIsHeld = false;
            Clear();
        }
        else if (control == MIDI_CC_RESET_ALL_CONTROLLERS && value == 0)
        {
            g_Pitchbend = PITCHBEND_MIDPOINT;
            
            // TODO: this code is duplicated. Maybe move it to a pedal function.
            g_PedalIsHeld = false;
            
            uint8_t i;
            
            for (i = 0; i < g_NumVoices; i++)
            {
                RefreshOutput(i);
            }
        }
        else if (control == MIDI_CC_ALL_NOTES_OFF && value == 0)
        {
            Clear();
        }
        
        return;
    }
    
    if (control == MIDI_CC_MOD)
    {
        if (g_NumVoices == 3)
        {
            code = value << 4; // approx 0-5V
            DAC_Set(3, code);
        }
    }
    else if (control == MIDI_CC_SUSTAIN)
    {
        // On if value > 63
        if (value & 0x40)
        {
            g_PedalIsHeld = true;
        }
        else
        {
            g_PedalIsHeld = false;
            
            uint8_t i;
            
            for (i = 0; i < g_NumVoices; i++)
            {
                RefreshOutput(i);
            }
        }
    }
}

inline void Poly1_Tick(void)
{
    
}

