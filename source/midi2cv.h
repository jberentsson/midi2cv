#ifndef MIDI2CV_H_
#define MIDI2CV_H_

#include <xc.h>

#define _XTAL_FREQ      8000000
#define NULL            0

#define PIN_B0          LATB0
#define PIN_B1          LATB1
#define PIN_B2          LATB2
#define PIN_B3          LATB3
#define PIN_B4          LATB4
#define PIN_B5          LATB5
#define PIN_B6          LATB6
#define PIN_B7          LATB7
#define PIN_A0          LATA0
#define PIN_A1          LATA1
#define PIN_A2          LATA2
#define PIN_A3          LATA3
#define PIN_A4          LATA4
#define PIN_A5          LATA5
#define PIN_A6          LATA6
#define PIN_A7          LATA7

typedef enum {false = 0, true = 1} bool_t;
typedef unsigned char uint8_t;
typedef signed char int8_t;
typedef unsigned short uint16_t;
typedef signed short int16_t;
typedef unsigned long uint32_t;
typedef signed long int32_t;


#endif

