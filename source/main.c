#include <xc.h>
#include "midi2cv.h"
#include "dac.h"
#include "midi.h"
#include "led.h"
#include "eeprom.h"


// CONFIG1
#pragma config "FCMEN = OFF"
#pragma config "IESO = ON"
#pragma config "CLKOUTEN = OFF"
#pragma config "BOREN = OFF"
#pragma config "CPD = OFF"
#pragma config "CP = OFF"
#pragma config "MCLRE = ON"
#pragma config "PWRTE = ON"
#pragma config "WDTE = OFF"
#pragma config "FOSC = INTOSC"

// CONFIG2
#pragma config "WRT = BOOT"
#pragma config "PLLEN = OFF"
#pragma config "STVREN = ON"
#pragma config "LVP = OFF"


#define MIDI_FIFO_LEN   16
#define MIDI_FIFO_MASK  0x0F

static uint8_t MidiFifo[MIDI_FIFO_LEN];
static uint8_t MidiFifoPut;
static uint8_t MidiFifoGet;

void interrupt ISR(void)
{
    if (TMR2IF)
    {
        TMR2IF = 0; // Clear interrupt flag
        MIDI_Tick();
    }

    if (RCIF)
    {
        // RCIF is cleared by reading RCREG
        uint8_t byte = RCREG;
        TXREG = byte;
        MidiFifo[MidiFifoPut] = byte;
        MidiFifoPut++;
        MidiFifoPut &= MIDI_FIFO_MASK;
    }
}

void main(void)
{
    di();

    OSCCON = 0b11110000; // 32MHz (8MHz with PLL)
    TRISA = 0b00100000; // Port A direction
    TRISB = 0b00000111; // Port B direction
    ANSELA = 0;
    ANSELB = 0;

    // Set up Timer2
    TMR2 = 0; // Clear counter
    TMR2IF = 0; // Clear interrupt flag
    // The timer block is clocked by Fosc/4, in our case 8 MHz.
    // That clock goes through a prescaler, which we set to 1:64 in T2CON.
    T2CON = 0b00000111;
    // So the timer is counting at 8 MHz / 64 = 125 kHz.
    // Set the period register to 124 in order to generate
    // interrupts at 1 kHz.
    PR2 = 124;
    TMR2IE = 1; // Enable Timer2 interrupt

    // Set up UART
    SPBRG = 0x3F; // Value for 31250 bauds:((CLK/(16 * BAUD) - 1))

    RXDTSEL = 1; // RB2 is UART RX
    TXCKSEL = 1; // RB5 is UART TX

    BRGH = 1; // High Speed
    SYNC = 0; // Asynchronous
    SPEN = 1; // Enable serial port pin
    CREN = 1; // Enable reception
    SREN = 0; // No effect
    RX9 = 0; // 8-bit reception
    // TXEN = 0; // Transmit disabled
    TXEN = 1; // Transmit enabled

    // Set up interrupts
    PEIE = 1; // Enables all periph interrupts
    RCIE = 1; // Enable RX interrupts
    TXIE = 0; // Disable TX interrupts

    PIN_A6 = 1;
    PIN_A6 = 0;

    EEPROM_Init();
    LED_Init();
    DAC_Init();
    MIDI_Init();

    MIDI_SetMode(PORTB & 3);

    MidiFifoPut = 0;
    MidiFifoGet = 0;
    ei();

    for (;;)
    {
        if (OERR)
        {
            // FIFO overload detection
            CREN = 0;
            CREN = 1;
            LED_Blink(5);
        }

        di();

        if (MidiFifoPut != MidiFifoGet)
        {
            // Data recieved
            uint8_t rx_data = MidiFifo[MidiFifoGet];
            MidiFifoGet++;
            MidiFifoGet &= MIDI_FIFO_MASK;

            PIN_A6 = 1;
            ei();

            MIDI_SetMode(PORTB & 3);
            MIDI_Process(rx_data);

            di();
            PIN_A6 = 0;
        }

        ei();
    }
}

