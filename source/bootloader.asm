#include "p16F1847.inc"

; *****************************************************************************

; For Bootloader located at program memory address 0, the application firmware must
; provide remapped reset and interrupt vectors outside of the Boot Block. The following
; #defines tell the bootloader firmware where application entry points are to be expected:
#define AppVector           0x200   ; remapped application start up code should be located here.
#define AppIntVector        0x204   ; remapped application interrupt handler should be located here

#define SYSEX_BEGIN         0xF0
#define SYSEX_END           0xF7
#define SYSEX_MFG_EDU	    0x7D
#define SYSEX_MIDI2CV_ID_1  0x25
#define SYSEX_MIDI2CV_ID_2  0x5E
#define COMMAND_RESET       .0
#define COMMAND_WRITEROW    .1
#define MAX_SYSEX_LENGTH    .71
#define DATA_LENGTH         .64

; *****************************************************************************

    __CONFIG _CONFIG1, _FCMEN_OFF & _IESO_ON & _CLKOUTEN_OFF & _BOREN_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDTE_OFF & _FOSC_INTOSC
    __CONFIG _CONFIG2, _WRT_BOOT & _PLLEN_OFF & _STVREN_ON & _LVP_OFF

; *****************************************************************************

; bank 0
g_DataBuffer    equ 0x20
g_SysExMfg      equ 0x20
g_SysExID1      equ 0x21
g_SysExID2      equ 0x22
g_SysExCommand  equ 0x23
g_SysExAddrH    equ 0x24
g_SysExAddrL    equ 0x25
g_SysExData     equ 0x26
g_SysExChecksum equ 0x66

; common ram
g_Counter       equ 0x70
g_AddressH      equ 0x71
g_AddressL      equ 0x72
g_NumBytes      equ 0x73
g_Checksum      equ 0x74
g_DelayCounter  equ 0x75
g_Timeout       equ 0x76
g_Pins          equ 0x77
g_DataH         equ 0x78
g_DataL         equ 0x79
g_Temp          equ 0x7F

; *****************************************************************************

    ; turn off bank warnings
    errorlevel -302

; *****************************************************************************

    ORG     0

Begin:
    nop ; required to allow debug executive startup when running under ICD
    goto    BootloaderEntryCheck

    ORG     0x0004

InterruptVector:
    ; set PCLATH for making a long jump to the AppIntVector address
    movlw   high(AppIntVector)
    movwf   PCLATH
    ; jump to remapped application interrupt vector.
    goto    AppIntVector

Delay100ms:
    movlw   .25
    call    Delayms
    movlw   .25
    call    Delayms
    movlw   .25
    call    Delayms
    movlw   .25
    call    Delayms
    return

Delayms:
    ; waits approx w milliseconds (assumes 32MHz, w < 32)
    movwf   g_DelayCounter
    lslf    g_DelayCounter, f
    lslf    g_DelayCounter, f
    lslf    g_DelayCounter, f
    clrw
    addlw   .1
    btfss   STATUS, C
    goto    $-2
    decf    g_DelayCounter, f
    btfss   STATUS, Z
    goto    $-6
    return

BootloaderEntryCheck:
    bcf     INTCON, GIE

    ; 32MHz (8MHz with PLL)
    banksel OSCCON
    movlw   b'11110000'
    movwf   OSCCON

    banksel TRISA
    movlw   b'00100000'
    movwf   TRISA
    movlw   b'00000111'
    movwf   TRISB

    banksel ANSELA
    clrf    ANSELA
    banksel ANSELB
    clrf    ANSELB

    ; start bootloader if B0 and B1 are both low
    banksel PORTB
    movf    PORTB, w
    andlw   0x03
    bz      StartBootloader

    ; Attempt to startup in Application mode.
    ; Read instruction at the application reset vector location.
    ; If we read 0x3FFF, assume that the application firmware has
    ; not been programmed yet, so don't try going into application mode.
    banksel EEADR
    movlw   low(AppVector)
    movwf   EEADRL
    movlw   high(AppVector)
    movwf   EEADRH
    movwf   PCLATH
    banksel EECON1
    bsf     EECON1, EEPGD
    bsf     EECON1, RD
    nop
    nop
    banksel EEDAT
    movf    EEDATH, w
    movwf   g_Temp
    movf    EEDATL, w

    xorlw   0xFF
    bnz     WatchSwitch
    movf    g_Temp, w
    xorlw   0x3F
    bnz     WatchSwitch
    goto    StartBootloader

; start bootloader if the mode switch is wiggled at startup
WatchSwitch:
    movlw   .175 ; 700ms
    movwf   g_Timeout
    movlw   .4
    movwf   g_Counter
    clrf    g_Pins
SwitchReadLoop:
    banksel LATA
    bsf     LATA, 6
    banksel PORTB
    movf    PORTB, w
    andlw   0x03
    movwf   g_Temp
    xorwf   g_Pins, w
    bz      SwitchNoChange

    decf    g_Counter, f
    bz      StartBootloader
    movf    g_Temp, w
    movwf   g_Pins
    ; fall thru...
SwitchNoChange:
    decf    g_Timeout, f
    bz      StartApplication
    movlw   .2
    call    Delayms
    bcf     LATA, 6
    movlw   .2
    call    Delayms
    goto    SwitchReadLoop

StartApplication:
    movlw   high(AppVector)
    movwf   PCLATH
    goto    AppVector

LightShow:
    banksel LATA
    bsf     LATB, 7
    call    Delay100ms
    bsf     LATB, 6
    call    Delay100ms
    bsf     LATB, 4
    call    Delay100ms
    bsf     LATB, 3
    call    Delay100ms
    bcf     LATB, 3
    call    Delay100ms
    bcf     LATB, 4
    call    Delay100ms
    bcf     LATB, 6
    call    Delay100ms
    bcf     LATB, 7
    bsf     LATA, 7
    return

StartBootloader:
    call    LightShow

    ; 31.25 kbaud
    banksel SPBRG
    clrf    SPBRGH
    movlw   0x3F
    movwf   SPBRGL

    banksel ANSELA
    clrf    ANSELA
    banksel ANSELB
    clrf    ANSELB

    ; TX pin is B5
	banksel APFCON1
	bsf     APFCON1, TXCKSEL
    banksel TRISB
    bcf     TRISB, TRISB5
    banksel TXSTA
    movlw   b'00100110'
    movwf   TXSTA

    ; RX pin is B2
	banksel APFCON0
	bsf     APFCON0, RXDTSEL
    banksel TRISB
    bsf     TRISB, TRISB2
    banksel RCSTA
    movlw   b'10010000'
    movwf   RCSTA

    goto    WaitForSysEx

; *****************************************************************************

MidiRx:
    banksel PIR1
    btfss   PIR1, RCIF
    goto    $-1
    banksel RCREG
    movf    RCREG, W
    return

MidiTx:
    banksel PIR1
    btfss   PIR1, TXIF
    goto    $-1
    banksel TXREG
    movwf   TXREG
    return

WaitForSysEx:
    call    MidiRx
    xorlw   SYSEX_BEGIN
    bnz     WaitForSysEx

    clrf    g_NumBytes
    clrf    g_Checksum
    movlw   g_DataBuffer
    movwf   FSR0L
    clrf    FSR0H

    movlw   0xFA
    call    MidiTx

SysExRxLoop:
    banksel LATA
    bcf     LATA, 7
    call    MidiRx
    movwf   INDF0
    banksel LATA
    bsf     LATA, 7

    ; switch to bank 0
    movlb   .0

    ; check for sysex end message
    xorlw   SYSEX_END
    bz      SysExEnd

    ; check for system realtime messages and ignore
    movf    INDF0, w
    andlw   0xF8
    xorlw   0xF8
    bz      SysExRxLoop

    ; check for invalid byte
    btfsc   INDF0, 7
    goto    UpdateFail

    ; ignore overflow
    movf    g_NumBytes, f
    xorlw   MAX_SYSEX_LENGTH
    bz      SysExRxLoop

    ; this byte is valid
    ; update checksum and increment counter, address
    movf    INDF0, w
    addwf   g_Checksum, f
    bcf     g_Checksum, 7
    incf    g_NumBytes, f
    incf    FSR0L, f
    goto    SysExRxLoop

SysExEnd:
    movlw   0xFB
    call    MidiTx

    ; switch to bank 0
    movlb   .0

    ; make sure checksum is 0
    movf    g_Checksum, w
    bnz     UpdateFail

    ; check ID bytes
    movf    g_SysExMfg, w
    xorlw   SYSEX_MFG_EDU
    bnz     WaitForSysEx

    movf    g_SysExID1, w
    xorlw   SYSEX_MIDI2CV_ID_1
    bnz     WaitForSysEx

    movf    g_SysExID2, w
    xorlw   SYSEX_MIDI2CV_ID_2
    bnz     WaitForSysEx

    ; check command byte
    movf    g_SysExCommand, w
    xorlw   COMMAND_WRITEROW
    bz      CommandWriteRow

    movf    g_SysExCommand, w
    xorlw   COMMAND_RESET
    reset

    goto    WaitForSysEx

CommandWriteRow:
    movlb   .0

    ; get address
    movf    g_SysExAddrH, w
    movwf   g_AddressH
    movf    g_SysExAddrL, w
    movwf   g_AddressL
    lsrf    g_AddressH, f
    btfsc   STATUS, C
    bsf     g_AddressL, 7

    call    FlashEraseRow
    call    FlashWriteRow
    goto    WaitForSysEx

UpdateFail:
    movlw   0xFF
    call    MidiTx
    call    LightShow
    goto    WaitForSysEx

; *****************************************************************************

FlashEraseRow:
    movlw   0xFC
    call    MidiTx
    movf    g_AddressH, w
    call    MidiTx
    movf    g_AddressL, w
    call    MidiTx

    ; set up address
    banksel EEADR
    movf    g_AddressH, w
    movwf   EEADRH
    movf    g_AddressL, w
    movwf   EEADRL

    banksel EECON1
    movlw   b'10010100'
    movwf   EECON1
    banksel EECON2
    movlw   0x55
    movwf   EECON2
    movlw   0xAA
    movwf   EECON2
    bsf     EECON1, WR
    nop
    nop
    btfsc   EECON1, FREE
    goto    $-1
    return

FlashWriteRow:
    movlw   0xFD
    call    MidiTx

    ; set up address
    banksel EEADR
    movf    g_AddressH, w
    movwf   EEADRH
    movf    g_AddressL, w
    movwf   EEADRL

    movlw   .32
    movwf   g_Counter

    banksel EECON1
    movlw   b'10100100'
    movwf   EECON1

    movlw   g_SysExData
    movwf   FSR0L
    clrf    FSR0H

FlashWriteLatchLoop:
    banksel EECON1
    movf    g_Counter, w
    xorlw   .1
    btfsc   STATUS, Z
    bcf     EECON1, LWLO

    ; set up data registers
    moviw   FSR0++
    movwf   g_DataH
    moviw   FSR0++
    movwf   g_DataL

    ; rightshift the high byte and set the MSbit in the low byte if needed
    lsrf    g_DataH, f
    btfsc   STATUS, C
    bsf     g_DataL, 7

    banksel EEDAT
    movf    g_DataH, w
    movwf   EEDATH
    movf    g_DataL, w
    movwf   EEDATL

    ; flash programming unlock sequence
    banksel EECON2
    movlw   0x55
    movwf   EECON2
    movlw   0xAA
    movwf   EECON2
    bsf     EECON1, WR
    nop
    nop
    btfsc   EECON1, WR
    goto    $-1

    ; increment write address and decrement counter
    banksel EEADR
    incf    EEADR, f
    decf    g_Counter, f
    bnz     FlashWriteLatchLoop

FlashWriteRowFinish:
    movlw   0xFE
    call    MidiTx
    return

; *****************************************************************************

    END
